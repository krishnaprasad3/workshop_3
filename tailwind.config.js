module.exports = {
    content: ["./src/**/*.{html,js}"],
    theme: {
        container: {
            // you can configure the container to be centered
            center: true,

            // default breakpoints but with 40px removed
            screens: {
                sm: "540px",
                md: "720px",
                lg: "960px",
                xl: "1140px",
                "2xl": "1420px",
            },
        },
        extend: {
            flex: {
                '2': '2 2 0%'
              },
            fontFamily: {
                rob: ["Roboto", "sans-serif"],
            },

            dropShadow: {
                "3xl": "0 35px 25px rgba(138, 43, 226, 0.25)",
            },
        },
    },
    plugins: [],
};
